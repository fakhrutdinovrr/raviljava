package com.company.validators;

public interface FormatValidator {
    boolean validate(String element);
}
