package com.company;

import com.company.repository.Repository;
import com.company.repository.impl.FileRepository;
import com.company.services.Service;
import com.company.services.impl.CalculatorService;
import com.company.util.Calculator;
import com.company.util.impl.CalculatorImpl;
import com.company.util.Converter;
import com.company.util.impl.ConverterImpl;
import com.company.validators.FormatValidator;
import com.company.validators.impl.RomanValidator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();

        FormatValidator validator = new RomanValidator();
        Converter converter = new ConverterImpl();
        Calculator calc = new CalculatorImpl(validator,converter);
        Repository repository = new FileRepository("Results");
        Service service = new CalculatorService(repository,calc);

        service.input(string);
//        System.out.println(calc.calc(string));

        repository.findByResult("0.0");


    }
}
