package ru.myprojects.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan (basePackages = "ru.myprojects")
public class ApplicationConfiguration {
}
