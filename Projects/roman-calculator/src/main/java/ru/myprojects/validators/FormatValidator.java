package ru.myprojects.validators;

public interface FormatValidator {
    boolean validate(String element);
}
