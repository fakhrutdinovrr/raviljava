package com.company.repository;

import java.util.Optional;

public interface Repository {
    void save(String inputData, String result);

    Optional<String> findByResult(String result);
}
