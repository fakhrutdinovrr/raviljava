package ru.myprojects.util.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.myprojects.util.Calculator;
import ru.myprojects.util.Converter;
import ru.myprojects.validators.FormatValidator;

@RequiredArgsConstructor
@Component
public class CalculatorImpl implements Calculator {
    private final FormatValidator romanValidator;
    private final Converter converter;


    @Override
    public String calc(String input) throws NumberFormatException {
        try {
            if (input.contains("+")) {
                String[] arrayOfSplit = input.split("\\+");
                for (int i = 0; i < arrayOfSplit.length; i++) {
                    if (romanValidator.validate(arrayOfSplit[0]) && romanValidator.validate(arrayOfSplit[1])) {
                        return converter.convertToRoman(converter.convertToArabian(arrayOfSplit[0]) + converter.convertToArabian(arrayOfSplit[1]));
                    }
                }
                return Double.toString(Double.parseDouble(arrayOfSplit[0]) + Double.parseDouble(arrayOfSplit[1]));
            } else if (input.contains("-")) {
                String[] arrayOfSplit = input.split("-");

                for (int i = 0; i < arrayOfSplit.length; i++) {
                    if (romanValidator.validate(arrayOfSplit[0]) && romanValidator.validate(arrayOfSplit[1])) {
                        return converter.convertToRoman(converter.convertToArabian(arrayOfSplit[0]) - converter.convertToArabian(arrayOfSplit[1]));
                    }
                }
                return Double.toString(Double.parseDouble(arrayOfSplit[0]) - Double.parseDouble(arrayOfSplit[1]));

            } else if (input.contains("*")) {
                String[] arrayOfSplit = input.split("\\*");

                for (int i = 0; i < arrayOfSplit.length; i++) {
                    if (romanValidator.validate(arrayOfSplit[0]) && romanValidator.validate(arrayOfSplit[1])) {
                        return converter.convertToRoman(converter.convertToArabian(arrayOfSplit[0]) * converter.convertToArabian(arrayOfSplit[1]));
                    }
                }
                return Double.toString(Double.parseDouble(arrayOfSplit[0]) * Double.parseDouble(arrayOfSplit[1]));
            } else if (input.contains("/")) {
                String[] arrayOfSplit = input.split("/");
                for (String s : arrayOfSplit) {
                    if (s.equals("0")) {
                        throw new ArithmeticException("cannot be divided by 0");
                    }
                    if (romanValidator.validate(arrayOfSplit[0]) && romanValidator.validate(arrayOfSplit[1])) {
                        return converter.convertToRoman(converter.convertToArabian(arrayOfSplit[0]) / converter.convertToArabian(arrayOfSplit[1]));
                    }
                }
                return Double.toString(Double.parseDouble(arrayOfSplit[0]) / Double.parseDouble(arrayOfSplit[1]));
            }
        }  catch (NumberFormatException e) {
            System.err.println("Please entry only Arabian or Roman numbers");
        }
        return null;
    }
}
