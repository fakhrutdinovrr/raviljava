package com.company.util.impl;

import com.company.util.Converter;

public class ConverterImpl implements Converter {
    @Override
    public String convertToRoman(int elements) {
        String[] roman = {"O", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX",
                "XXI", "XXII", "XXIII", "XXIV", "XXV", "XXVI", "XXVII", "XXVIII", "XXIX", "XXX", "XXXI", "XXXII", "XXXIII", "XXXIV", "XXXV", "XXXVI", "XXXVII", "XXXVIII", "XXXIX", "XL",
                "XLI", "XLII", "XLIII", "XLIV", "XLV", "XLVI", "XLVII", "XLVIII", "XLIX", "L", "LI", "LII", "LIII", "LIV", "LV", "LVI", "LVII", "LVIII", "LIX", "LX",
                "LXI", "LXII", "LXIII", "LXIV", "LXV", "LXVI", "LXVII", "LXVIII", "LXIX", "LXX",
                "LXXI", "LXXII", "LXXIII", "LXXIV", "LXXV", "LXXVI", "LXXVII", "LXXVIII", "LXXIX", "LXXX",
                "LXXXI", "LXXXII", "LXXXIII", "LXXXIV", "LXXXV", "LXXXVI", "LXXXVII", "LXXXVIII", "LXXXIX", "XC",
                "XCI", "XCII", "XCIII", "XCIV", "XCV", "XCVI", "XCVII", "XCVIII", "XCIX", "C"
        };
        return roman[elements];
    }

    @Override
    public int convertToArabian(String elements) {
        char I = 'I';
        char V = 'V';
        char X = 'X';
        char L = 'L';
        char C = 'C';
        char D = 'D';
        char M = 'M';

        char[] words = elements.toCharArray();
        int result = 0;

        for (int i = 0; i < words.length; i++) {
            if (i + 1 < words.length) {
                if (words[i] == I && i + 1 <= words.length && words[i + 1] == V) {
                    result += 4;
                    i++;
                    continue;
                } else if (words[i] == X && words[i + 1] == L) {
                    result += 40;
                    i++;
                    continue;
                } else if (words[i] == X && words[i + 1] == C) {
                    result += 90;
                    i++;
                    continue;
                } else if (words[i] == C && words[i + 1] == M) {
                    result += 900;
                    i++;
                    continue;
                } else if (words[i] == I && words[i + 1] == X) {
                    result += 9;
                    i++;
                    continue;
                } else if (words[i] == C && words[i + 1] == D) {
                    result += 400;
                    i++;
                    continue;
                }
            }
            if (words[i] == I) {
                result += 1;
            } else if (words[i] == V) {
                result += 5;
            } else if (words[i] == X) {
                result += 10;
            } else if (words[i] == L) {
                result += 50;
            } else if (words[i] == C) {
                result += 100;
            } else if (words[i] == D) {
                result += 500;
            } else if (words[i] == M) {
                result += 1000;
            }
        }
        return result;
    }
}
