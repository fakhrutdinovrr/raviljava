package com.company.services.impl;

import com.company.services.Service;
import com.company.util.Calculator;
import com.company.repository.Repository;

public class CalculatorService implements Service {
    private final Repository fileRepository;
    private final Calculator calculator;

    public CalculatorService(Repository fileRepository, Calculator calculator) {
        this.fileRepository = fileRepository;
        this.calculator = calculator;
    }

    @Override
    public void input(String string) {
        fileRepository.save(string,calculator.calc(string));
    }
}
