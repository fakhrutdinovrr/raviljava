package com.company.util;

public interface Converter {
    String convertToRoman(int elements);

    int convertToArabian(String elements);
}
