package com.company.repository.impl;

import com.company.repository.Repository;
import com.company.repository.exeption.ResultNotFound;

import java.io.*;
import java.util.Optional;

public class FileRepository implements Repository {

    private final String fileName;

    public FileRepository(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void save(String inputData, String result) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));){
            writer.write(inputData + "=" + result);
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<String> findByResult(String result) throws ResultNotFound {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String readLine = reader.readLine();
            while (readLine != null) {
                String[] array = readLine.split("=");
                if (array[1].equals(result)) {
                    System.out.println(readLine);
                }
                readLine = reader.readLine();
            }
        } catch (IOException e) {
            throw new ResultNotFound();
        }
        return Optional.empty();
    }
}
