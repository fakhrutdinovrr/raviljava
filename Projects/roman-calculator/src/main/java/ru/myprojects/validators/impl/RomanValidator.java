package ru.myprojects.validators.impl;

import org.springframework.stereotype.Component;
import ru.myprojects.validators.FormatValidator;

import java.util.ArrayList;
import java.util.List;


@Component
public class RomanValidator implements FormatValidator {
    @Override
    public boolean validate(String element) {
        char[] array = element.toCharArray();
        List<Character> list = new ArrayList<Character>();
        list.add('I');
        list.add('V');
        list.add('X');
        list.add('L');
        list.add('C');
        list.add('D');
        list.add('M');

        for (char temp : array) {
            if (list.contains(temp)) {
                return true;
            }
        }
    return false;
    }
}
