package ru.myprojects;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.myprojects.config.ApplicationConfiguration;
import ru.myprojects.util.Calculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
        Calculator calculator = context.getBean(Calculator.class);

        Scanner scanner = new Scanner(System.in);

        String string = scanner.nextLine();

        System.out.println(calculator.calc(string));
    }
}
